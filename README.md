# ranger-vcsfolder

This plugin add a new variable to ranger context for colorscheme called `vcsfolder`.    

## Installation
Just copy `ranger-vcsfolder.py` to `${XDG_CONFIG_HOME}/ranger/plugins/`

## Usage
```python
from ranger.colorschemes.default import Default
from ranger.gui.color import red

class Scheme(Default):
    def use(self, context):
        fg, bg, attr = Default.use(self, context)

        if(context.vcsfolder and not context.selected):
            fg = red

        return fg, bg, attr
```

## License
[GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/)
