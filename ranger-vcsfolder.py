import ranger.gui.context
import ranger.gui.widgets.browsercolumn
import os.path

ranger.gui.context.CONTEXT_KEYS.append("vcsfolder")
ranger.gui.context.Context.vcsfolder = False

OLD_HOOK_BEFORE_DRAWING = ranger.gui.widgets.browsercolumn.hook_before_drawing

def contain_vcs_folder(fsobject):
    vcs_folder = (".git", ".hg", ".svn", ".bzr")

    for i in vcs_folder:
        if(os.path.isdir(fsobject.realpath+"/"+i)):
            return True
    return False

def new_hook(fsobject, color_list):
    if(contain_vcs_folder(fsobject)):
        color_list.append('vcsfolder')
    return OLD_HOOK_BEFORE_DRAWING(fsobject, color_list)

ranger.gui.widgets.browsercolumn.hook_before_drawing = new_hook
